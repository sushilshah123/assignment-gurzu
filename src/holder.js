import React from 'react'
import {useState,useEffect} from 'react'
import Data from '../data/item.json'
import ItemDisplay from './ItemDisplay'
import './Item.css'

export default function Items() {
    const[items,setItems]=useState([])
    const[isform,setForm]=useState(false)
    const [inputitem,setInputitem]=useState('')
    const[price,setPrice]=useState('')

    useEffect(()=>{
          setItems(Data)
    },[])

    function handleClick(){
      setForm(true)

    }
    function handleSubmit(){
      setForm(false)
      const itemname=inputitem
      const itemprice=price
      const number=Math.floor(Math.random() * 11);
     const newItem={id:number,title:itemname,price:itemprice,count:0,total:0,info:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged."}
      setItems([...items,newItem])
      setInputitem('')
      setPrice('')
    }
    function handleChangeItem(e){
         const item=e.target.value
         setInputitem(item)
    }
    function handlePrice(e){
      const price=e.target.value
      setPrice(price)
    }
    function handleDelete(id){
         const tempItems=items
         const newItems=tempItems.filter(item=>id!==item.id)
         setItems(newItems)
    }
    return (
        <div class=" container item__container">
        <div class="item__main mt-4 pt-2">
           <h4>Added Items</h4>
           <div class="card item__order pb-3 mb-5">
            <h4>Order number <span class="item__ordernumber">#22540</span></h4>
            <div class="row">
          {items.map((item)=>{
            return(
                <div class="col-md-12 my-3">
                      <ItemDisplay item={item} handleDelete={handleDelete}/>
                 </div>
                )
          
          })}
            </div>
            <div>
            {isform && 
              <div>
              <form>
              <div class="form-group">
                 <input placeholder="Enter Item Name" type="text" class="form-control" onChange={handleChangeItem}/>
              </div>
              <div class="form-group">
              <input placeholder="Enter Price" type="number" class="form-control" onChange={handlePrice}/>
              </div>
                 <button class="btn btn-primary mb-3" onClick={handleSubmit}>Submit</button>
              </form>
              </div>
            }
            </div>
            <button class="add__item" onClick={handleClick}><i class='bx bx-plus pr-3'></i>Add more items</button>
            </div>
          </div>
          <h4 class="mb-3">Previously ordered items</h4>
           <div class="previous__item__container">
           <div class="row previous__item__nav">
           <div class="col-md-9 previous__item__header">
           <h5>Order number #58487</h5>
           <h6>APR 10,2020</h6>
           </div>
           <div class="col-md-3 previous__item__price">$70.5<i class='bx bxs-chevron-up pl-4' ></i></div>
           </div>
           <div class="row mt-4 previous__item__order">
           <div class="col-md-5">
           <span class="item__footer">Items</span>
           <h5>Red N Hot(Thin Crust)Pizza</h5>
           <h5>Vggie Supreme Pizza</h5>
           <h5>Oven-Baked potatoes Fries</h5>
           </div>
           <div class="col-md-1">
           <span class="item__footer">Qnt.</span>
           <h5>2</h5>
           <h5>2</h5>
           <h5>2</h5>
           </div>
           <div class="col-md-3">
           <span class="item__footer">Restaurant</span>
           <h5>Pizza Hut</h5>
           <span class="item__footer">Rider</span>
           <h5>Matthew</h5>
           </div>
           <div class="col-md-3">
           <span class="item__footer">Total Price</span>
           <h4>$70.50</h4>
           <button>Order Details</button>
           </div>
           </div>
           </div>
        </div>
    )
}
