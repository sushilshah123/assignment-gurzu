import React from 'react'
import './App.css';
import Sidebar from './components/Sidebar'
import Body from './components/Body'
import Context from './components/Context'
import {Route,Switch} from 'react-router-dom'
import Detail from './components/Detail'

function App() {
  return (
    <Context>
    <div className="App">
     <div class="main__container">
     <Sidebar/>
     <Switch>
         <Route exact path="/" component={Body}/>
         <Route path="/detail" component={Detail}/>
     </Switch>
     </div>
    </div>

    </Context>
  );
}

export default App;
