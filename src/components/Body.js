import React from 'react'
import Items from './Items'
import Payment from './Payment'

export default function Body() {
    return (
        <div class="container-fluid item__container">
        <div class="row">
          <div class="col-sm-9 col-md-8">
          <Items/>
          </div>
          <div class="col-sm-3 col-md-4">
          <Payment/>
          </div>
        </div>
          
        </div>
    )
}
