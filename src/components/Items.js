import React from 'react'
import {useState,useEffect} from 'react'
import ItemDisplay from './ItemDisplay'
import './Item.css'
import {useProvideContext} from './Context'

export default function Items() {
  const contextValue=useProvideContext();
  const items=contextValue.items
  const handleDelete=contextValue.handleDelete
  const isform=contextValue.isform
  const handleClick=contextValue.handleClick
  const handleSubmit=contextValue.handleSubmit
  const handlePrice=contextValue.handlePrice
  const handleChangeItem=contextValue.handleChangeItem
  const increment=contextValue.increment
  const decrement=contextValue.decrement
  const handleDetail=contextValue.handleDetail
    return (
        <div class="container-fluid item__container">
        <div class="item__main mt-4 pt-2">
           <h4 class="previous">Added Items</h4>
           <div class="item__order pb-3 mb-5">
            <h4>Order number <span class="item__ordernumber">#22540</span></h4>
            <div class="row">
          {items.map((item)=>{
            return(
                <div class="col-md-12 my-3">
                      <ItemDisplay item={item} handleDelete={handleDelete} increment={increment} decrement={decrement} handleDetail={handleDetail}/>
                 </div>
                )
          
          })}
            </div>
            <div>
            {isform && 
              <div>
              <form>
              <div class="form-group">
                 <input placeholder="Enter Item Name" type="text" class="form-control" onChange={handleChangeItem}/>
              </div>
              <div class="form-group">
              <input placeholder="Enter Price" type="number" class="form-control" onChange={handlePrice}/>
              </div>
                 <button class="btn btn-primary mb-3" onClick={handleSubmit}>Submit</button>
              </form>
              </div>
            }
            </div>
            <button class="add__item" onClick={handleClick}><i class='bx bx-plus pr-3'></i>Add more items</button>
            </div>
          </div>
          <h4 class="mb-3 previous">Previously ordered items</h4>
           <div class="previous__item__container mb-3 pb-3">
           <div class="row previous__item__nav mb-3 pb-4">
           <div class="col-8 col-md-9 previous__item__header">
           <h5>Order number #58487</h5>
           <h6>APR 10,2020</h6>
           </div>
           <div class="col-4 col-md-3 previous__item__price">$70.5<i class='bx bxs-chevron-up pl-4' ></i></div>
           </div>
           <div class="row mt-4 previous__item__order">
           <div class="col-10 col-sm-6 col-md-5">
           <span class="item__footer">Items</span>
           <h5>Red N Hot(Thin Crust)Pizza</h5>
           <h5>Vggie Supreme Pizza</h5>
           <h5>Oven-Baked potatoes Fries</h5>
           </div>
           <div class="col-2 col-md-1">
           <span class="item__footer">Qnt.</span>
           <h5>2</h5>
           <h5>2</h5>
           <h5>2</h5>
           </div>
           <div class="col-md-3">
           <span class="item__footer">Restaurant</span>
           <h5>Pizza Hut</h5>
           <span class="item__footer">Rider</span>
           <h5>Matthew</h5>
           </div>
           <div class="col-md-3">
           <span class="item__footer">Total Price</span>
           <h4>$70.50</h4>
           <button>Order Details</button>
           </div>
           </div>
           </div>
        </div>
    )
}
