import React from 'react'
import './Payment.css'
import {useProvideContext} from './Context'

export default function Payment() {
    const contextValue=useProvideContext();
    const total=contextValue.total
    const delivery=contextValue.delivery
    const coupon=contextValue.coupon
    const subtotal=contextValue.subtotal
    return (
        <div class="pt-3">
           <div class=" mt-5 payment__conntainer">
           <div class="row my-3 py-2">
           <div class="col-2 col-md-2"> <i class='bx bx-location-plus pt-4' ></i></div>
           <div class="col-7 col-md-8">
           <span class="age">Delivery address</span>
           <h5>2510 E Sunset Rd Ste 5,NY 89120 </h5>
           </div>
           <div class="col-3 col-md-2">
           <i class='bx bx-edit-alt' ></i>
           </div>
           </div>
           <div class="row my-3 py-2">
           <div class="col-9 col-md-10">
           <h5>Choose payment option</h5>
            <form>
               <div class="form-check">
               <input type="checkbox" class="form-check-input" id="exampleCheck1"/>
               <i class='bx bxs-credit-card-alt mr-3' ></i>
               <label class="form-check-label" for="exampleCheck1">Credit Card</label>
               </div>
            </form>
            <form>
            <div class="form-check">
            <input type="checkbox" class="form-check-input" id="exampleCheck1"/>
            <i class='bx bxl-paypal mr-3' ></i>
            <label class="form-check-label" for="exampleCheck1">PayPal</label>
            </div>
           </form>
          
           </div>
           <div class="col-3 col-md-2">
           <i class='bx bx-edit-alt' ></i>
           </div>
           </div>
           <div class="row my-3 py-2">
           <div class="col-9 col-md-10">
           <h5 class="mb-2 pb-2">Rider Information</h5>
           <div class="image">
           <img src="/sushil.jpg" class="mr-3"/>
           <div>
           <h6>Mark alex</h6>
           <h6><span class="pr-2 age">Age:</span>22</h6>
           </div>
           </div>
           </div>
           <div class="col-3 col-md-2">
           <i class='bx bx-phone' ></i>
           </div>
           </div>
           <div class="row my-3 py-2">
           <div class="col-8 col-md-8 payable">
           <h5 class="pb-3">Payable amount</h5>
           <h6>Sub total</h6>
           <h6>Delivery charge</h6>
           <h6>Cupon applied</h6>
           <h5 class="mt-3 pt-2">Grand Total</h5>
           </div>
             <div class="col-4 col-md-4 payable total">
           <h6 class="pt-4 mt-4">${subtotal}</h6>
           <h6>${delivery}</h6>
           <h6>${coupon}</h6>
           <h5 class="mt-3 pt-2">${total}</h5>
           </div>
           <div class="row mt-3 pt-2 text-center">
           <div class="col-md-9 ml-5">
           <button>Conform order and pay</button>
           </div>
           </div>
           </div>
            </div>
        </div>
    )
}
