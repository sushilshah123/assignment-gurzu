import React from 'react'
import './Item.css'
import {Link} from 'react-router-dom'

export default function ItemDisplay(props) {
    const{item,handleDelete,increment,decrement,handleDetail}=props
    return (
        
        <div class="row item__detail">
        <div class="col-sm-7 col-md-6">
        <Link to="/detail"> <span onClick={()=>handleDetail(item.id)}>{item.title}</span></Link>
        <div>
        
        <div class="mt-2">
        <span class="mr-3 pr-2 item__price">${item.price}</span>
        <span>Edit</span>
        </div>
      
        </div>
        </div>
        <div class="col-sm-3 col-md-3 item__number">
        <button onClick={()=>decrement(item.idid)}>-</button>
        <span class="px-2">{item.count}</span>
        <button onClick={()=>increment(item.id)}>+</button>
        <div class="item__footer pt-2">Quantity</div>
        </div>
        <div class="col-sm-1 col-md-2">
         <span>  ${item.total}</span>
         <div class="item__footer pt-2">subtotal</div>
        </div>
        <div class="col-sm-1 col-md-1">
        <i class='bx bx-x' onClick={()=>handleDelete(item.id)}></i>
        </div>
        </div>
    )
}
