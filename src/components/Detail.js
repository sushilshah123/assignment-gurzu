import React from 'react'
import {useProvideContext} from './Context'
import './Detail.css'
import{Link} from 'react-router-dom'

export default function Detail() {
    const contextValue=useProvideContext();
    const detail=contextValue.detail
    console.log("hello from detail")
    return (
        <div class="container maincont">
          <div class="row">.
          <div class="col-md-12">
          <div class="row">
          <div class="col-md-4">
          <h3>{detail.title}</h3>
          </div>
          <div class="col-md-8">
          <div>
          <h4>Price: ${detail.price}</h4>
          <h4 class="pt-3">{detail.info}</h4>
          </div>
       <Link to="/"><button class=" btn btn-warning mt-4">Back to home</button></Link>
          </div>
          </div>
          </div>
          </div>   
        </div>
    )
}
