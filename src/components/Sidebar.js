import React from 'react'
import './Sidebar.css'

export default function Sidebar() {
    return (
        <div class="sidebar__container">
            <h3>Foodholic</h3>
            <div class="sidebar__class">
            <div class="sidebar__menu">
            <h4><i class='bx bxs-home'></i>Home</h4>
            <h4><i class='bx bxs-cart' ></i>My Cart</h4>
            <h4><i class='bx bx-history' ></i>History</h4>
            <h4><i class='bx bx-restaurant' ></i>Restaurants</h4>
            <h4><i class='bx bx-gift' ></i>Offers</h4>
            </div>
            <div class="sidebar__footer">
            <div class="sidebar__footer__com">
            <img src="/sushil.jpg"/>
            <h4 class="pl-2 ml-2">Alex Ferguson</h4>
            <i class='bx bxs-chevron-up pl-4' ></i>
            </div>
            <h4 class=" customer mb-3 pb-4"><i class='bx bx-chat mr-2'></i>Customer Support</h4>
            </div>
            </div>
        </div>
    )
}
