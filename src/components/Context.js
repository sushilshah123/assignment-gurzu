import React from 'react'
import {createContext,useContext} from 'react'
import {useState,useEffect} from 'react'
import Data from '../data/item.json'
   
const contextvalue=createContext('')

export const useProvideContext=()=>{
    const contextConsumer=useContext(contextvalue)
    return contextConsumer;
}

export default function Context(props) {
    const[items,setItems]=useState([])
    const[isform,setForm]=useState(false)
    const [inputitem,setInputitem]=useState('')
    const[price,setPrice]=useState('')
    const[delivery,setDelivery]=useState(120)
    const[coupon,setCoupon]=useState(50)
    const [total,setTotal]=useState('')
    const[subtotal,setSubtotal]=useState('')
    const[detail,setDetail]=useState('')
    console.log(detail)

    useEffect(()=>{
          setItems(Data)
          addTotal()
    },[items])

    function handleClick(){
      setForm(true)

    }
    function handleSubmit(){
      setForm(false)
      const itemname=inputitem
      const itemprice=price
      const number=Math.floor(Math.random() * 11);
     const newItem={id:number,title:itemname,price:itemprice,count:0,total:price,info:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged."}
      setItems([...items,newItem])
      setInputitem('')
      setPrice('')
    }
    function handleChangeItem(e){
         const item=e.target.value
         setInputitem(item)
    }
    function handlePrice(e){
      const price=e.target.value
      setPrice(price)
    }
    function handleDelete(id){
         const tempItems=items
         const newItems=tempItems.filter(item=>id!==item.id)
         setItems(newItems)
    }
    function increment(id){
        const  tempItems=items
        const selected=tempItems.find(item=>item.id===id)
        const index=tempItems.indexOf(selected)
        const product=tempItems[index]
        product.count=product.count+1
        product.total=product.count*product.price
        setItems([...tempItems])
    }
    function decrement(id){
        const tempItems=items
        const selected=tempItems.find(item=>item.id===id)
        const index=tempItems.indexOf(selected)
        const product=tempItems[index]
        product.count=product.count-1
        if(product.count===0)
        {
            handleDelete(id)
        }
       
        else{
            product.total=product.count*product.price
            setItems([...tempItems])
        }  
    }

    function addTotal(){
        let subtotal=0;
        items.map(item=>subtotal=subtotal+item.total)
        const Total=subtotal+delivery+coupon
         setTotal(Total)
         setSubtotal(subtotal)
    }
    function handleDetail(id){
        const temproducts=items;
        const itemproduct=temproducts.find(item=>item.id===id)
        setDetail(itemproduct)
    }

    return (
        <contextvalue.Provider value={{
            items,
            inputitem,
            price,
            isform,
            handleClick:handleClick,
            handleDelete:handleDelete,
            handlePrice:handlePrice,
            handleChangeItem:handleChangeItem,
            handleSubmit:handleSubmit,
            increment:increment,
            decrement:decrement,
            total,
            delivery,
            coupon,
            subtotal,
            detail,
            handleDetail:handleDetail

        }}>
            {props.children}
        </contextvalue.Provider>
    )
}
